<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Loads the user for the given username.
     *[{"data":[{"id":5,"full_name":"Jo\u00e3o Marcus"},{"id":6,"full_name":"JOAO MARCUS DE JESUS"}]}]
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder("u")
            ->where("u.email = :email")
            ->andWhere("u.status = :status")
            ->setParameter('email', $username)
            ->setParameter('status', true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function transform(User $user)
    {
        return [
            'id'    => (int) $user->getId(),
            'full_name' => (string) $user->getFullName(),
        ];
    }

    public function transformAll()
    {
        $users = $this->findAll();

        $usersArray = [];

        foreach ($users as $user) {
            $usersArray[] = $this->transform($user);
        }
        return $usersArray;
    }
}
                                