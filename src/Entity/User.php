<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email", message="Esse e-mail já está em uso.")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $first_name;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $last_name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $email;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $password;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=32)
     */
    private $token;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $created_at;
    
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated_at;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $status = false;
    
    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getFirstName() : ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name) : self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName() : ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name) : self
    {
        $this->last_name = $last_name;

        return $this;

    }

    public function getEmail() : ?string
    {
        return $this->email;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;

    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token) : self
    {
        $this->token = $token;

        return $this;

    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at) : self
    {
        $this->created_at = $created_at;

        return $this;

    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at) : self
    {
        $this->updated_at = $updated_at;

        return $this;

    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;

    }

    public function getStatus()
    {
        return $this->status;
    }


    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles) : self
    {
        $this->roles[] = $roles;
        
        return $this;
    }

    public function clearRoles(): self
    {
        $this->roles = [];
        
        return $this;
    }

    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function setPassword($password) : self
    {
        $this->password = $password;

        return $this;

    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->email;
    }
    
    public function eraseCredentials()
    {
        return null;
    }

    public function passwordEncode($encoder, $password)
    {
        $password_bycrpt = $encoder->encodePassword($this, $password);

        return $password_bycrpt;
    }

    public function generateToken()
    {
        $token = md5(uniqid());

        return $token;
    }

    public function getFullName()
    {
        return "{$this->getFirstName()} {$this->getLastName()}";
    }

    public function getStatusName()
    {
        return $this->getStatus() ? 'Ativo' : 'Desativado';
    }

    public function getRolesName()
    {
        $roles = $this->getRoles();

        $name = '';

        foreach ($roles as $role) {
            
            switch ($role) {
                case 'ROLE_ADMIN':
                    $name = 'Administrador';
                    break;
                case 'ROLE_USER':
                    $name = 'Usuário';
                    break;
                case 'ROLE_GESTOR':
                    $name = 'Gestor';
                    break;
            }

            return $name;
        }
    }
}
