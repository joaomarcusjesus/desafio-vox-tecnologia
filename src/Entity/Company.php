<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 * @UniqueEntity("email", message="Esse e-mail já está em uso.")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Campo é obrigatório")
     * @ORM\Column(type="string", length=255)
     */
    
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(message="Campo é obrigatório")
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="Campo é obrigatório")
     * @ORM\Column(type="string", length=255)
     */

    private $responsible;

    /**
     * @var string
     * @Assert\NotBlank(message="Campo é obrigatório")
     * @ORM\Column(type="string", length=100, unique=true)
     */
    
    private $fantasy;

    /**
     * @ORM\OneToMany(targetEntity="\App\Entity\Owner", mappedBy="company")
     */
    private $owners;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $status = false;

    public function __construct()
    {
        $this->owners = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getResponsible(): ?string
    {
        return $this->responsible;
    }

    public function setResponsible(string $responsible): self
    {
        $this->responsible = $responsible;

        return $this;
    }

    public function getFantasy(): ?string
    {
        return $this->fantasy;
        
    }

    public function setFantasy(string $fantasy): self
    {
        $this->fantasy = $fantasy;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusName()
    {
        return $this->getStatus() ? 'Ativo' : 'Desativado';
    }


}
