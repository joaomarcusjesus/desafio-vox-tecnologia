<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnerRepository")
 * @UniqueEntity("email", message="Esse e-mail já está em uso.")
 */
class Owner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $first_name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $last_name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $email;

    /**
     * Many owners have one company. This is the owning side.
     * @ORM\ManyToOne(targetEntity="\App\Entity\Company", inversedBy="owners")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Campo é obrigatório")
     */
    private $company;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $status = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }


    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;
        
        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }
    
    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getFullName()
    {
        return "{$this->getFirstName()} {$this->getLastName()}";
    }

    public function getStatusName()
    {
        return $this->getStatus() ? 'Ativo' : 'Desativado';
    }

    public function getCompanyName()
    {
        return $this->company->getName();
    }
}
