<?php

namespace App\Controller\Admin\Companies;

use App\Entity\Company;
use App\Form\CompanyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @IsGranted("ROLE_ADMIN")
 */
class CompanyController extends Controller
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        // Dependency Injection
        $this->em = $em;
    }

    /**
     * @Route("/admin/companies", name="admin.companies.index")
     * @Template("admin/companies/index.html.twig")
     */
    public function index()
    {
        $results = $this->em->getRepository(Company::class)->findAll();

        return [
            'results' => $results
        ];

    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return array
     * @Route("/admin/companies/visualizar/{id}", name="admin.companies.show")
     * @Template("/admin/companies/show.html.twig")
     */
    public function show(Request $request, $id)
    {
        $result = $this->em->getRepository(Company::class)->find($id);

        return [
            'result' => $result
        ];
    }


    /**
     * @Route("/admin/companies/create", name="admin.companies.create")
     * @Template("admin/companies/create.html.twig")
     */
    public function create(Request $request)
    {
        $company = new Company();

        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Persist
            $this->em->persist($company);
            $this->em->flush();

            // Redirect
            return $this->redirect('/admin/companies');
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @Template("/admin/companies/edit.html.twig")
     * @Route("/admin/companies/edit/{id}", name="admin.companies.edit")
     */
    public function update(Request $request, $id)
    {
        $company = $this->em->getRepository(Company::class)->find($id);

        $form = $this->createForm(CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($company);
            $this->em->flush();

            return $this->redirect('/admin/companies');
        }

        return [
            'company' => $company,
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @Route("admin/companies/delete/{id}", name="admin.companies.delete")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, $id)
    {

        $company = $this->em->getRepository(Company::class)->find($id);

        if (!$company) {
            // Todo alert
        } else {
            $this->em->remove($company);
            $this->em->flush();
        }

        return $this->redirect("/admin/companies");
    }


}