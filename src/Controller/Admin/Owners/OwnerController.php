<?php

namespace App\Controller\Admin\Owners;

use App\Entity\Owner;
use App\Form\OwnerType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class OwnerController extends Controller
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        // Dependency Injection
        $this->em = $em;
    }

    /**
     * @Route("/admin/owners", name="admin.owners.index")
     * @Template("admin/owners/index.html.twig")
     */
    public function index()
    {
        $results = $this->em->getRepository(Owner::class)->findAll();

        return [
            'results' => $results
        ];

    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return array
     * @Route("/admin/owners/visualizar/{id}", name="admin.owners.show")
     * @Template("/admin/owners/show.html.twig")
     */
    public function show(Request $request, $id)
    {
        $result = $this->em->getRepository(Owner::class)->find($id);

        return [
            'result' => $result
        ];
    }


    /**
     * @Route("/admin/owners/create", name="admin.owners.create")
     * @Template("admin/owners/create.html.twig")
     */
    public function create(Request $request)
    {
        $owner = new Owner();

        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Persist
            $this->em->persist($owner);
            $this->em->flush();

            // Redirect
            return $this->redirect('/admin/owners');
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @Template("/admin/owners/edit.html.twig")
     * @Route("/admin/owners/edit/{id}", name="admin.owners.edit")
     */
    public function update(Request $request, $id)
    {
        $owner = $this->em->getRepository(Owner::class)->find($id);

        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($owner);
            $this->em->flush();

            return $this->redirect('/admin/owners');
        }

        return [
            '$owner' => $owner,
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @Route("admin/owners/delete/{id}", name="admin.owners.delete")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, $id)
    {

        $owner = $this->em->getRepository(Owner::class)->find($id);

        if (!$owner) {
            // Todo alert
        } else {
            $this->em->remove($owner);
            $this->em->flush();
        }

        return $this->redirect("/admin/owners");
    }


}