<?php

namespace App\Controller\Admin\Users;

use App\Entity\User;
use App\Form\UserAdminCreateType;
use App\Form\UserAdminEditType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
  * @IsGranted("ROLE_ADMIN")
 */
class UserController extends Controller
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        // Dependency Injection
        $this->em = $em;
    }
    
    /**
     * @Route("/admin/users", name="admin.users.index")
     * @Template("admin/users/index.html.twig")
     */
    public function index()
    {
        $results = $this->em->getRepository(User::class)->findAll();

        return [
            'results' => $results
        ];
        
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return array
     * @Route("/admin/users/visualizar/{id}", name="admin.users.show")
     * @Template("/admin/users/show.html.twig")
     */
    public function show(Request $request, $id)
    {
        $result = $this->em->getRepository(User::class)->find($id);

        return [
            'result' => $result
        ];
    }


    /**
     * @Route("/admin/users/create", name="admin.users.create")
     * @Template("admin/users/create.html.twig")
     */
    public function create(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserAdminCreateType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Encode
            $user->setPassword($user->passwordEncode($this->get('security.password_encoder')
                , $form->getData()->getPassword()));

            // Token
            $user->setToken($user->generateToken());

            // Persist
            $this->em->persist($user);
            $this->em->flush();

            // Flash
            $this->addFlash('success', 'Cadastrado com sucesso!');

            // Redirect
            return $this->redirect('/admin/users');
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @Template("/admin/users/edit.html.twig")
     * @Route("/admin/edit/{id}", name="admin.users.edit")
     */
    public function update(Request $request, $id)
    {
        $user = $this->em->getRepository(User::class)->find($id);

        $form = $this->createForm(UserAdminEditType::class, $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($user);
            $this->em->flush();
            
            return $this->redirect('/admin/users');
        }
        
        return [
            'user' => $user,
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @Route("admin/users/delete/{id}", name="admin.users.delete")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Request $request, $id)
    {

        $user = $this->em->getRepository(User::class)->find($id);
        
        if (!$user) {
           // Todo alert
        } else {
            $this->em->remove($user);
            $this->em->flush();
        }

        return $this->redirect("/admin/users");
    }


}