<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class UserController
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        // Dependency Injection
        $this->em = $em;
    }

    /**
     * @Route("/api/all-users/")
     */
    public function allUsers(UserRepository $repository)
    {

        $results = $repository->transformAll();

        return new JsonResponse([
            [
                'data' => $results
            ]
        ]);
    }
}