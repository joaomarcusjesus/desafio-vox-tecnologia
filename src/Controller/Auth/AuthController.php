<?php

namespace App\Controller\Auth;

use App\Entity\User;
use App\Form\UserFrontType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends Controller
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        // Dependency Injection
        $this->em = $em;
    }

    /**
     * @Route("/acessar", name="auth.login")
     * @Template("auth/login.html.twig")
     */
    public function login(Request $request, AuthenticationUtils $utils)
    {
       $error = $utils->getLastAuthenticationError();

       $last_username = $utils->getLastUsername();

       return [
           'last_username' => $last_username,
           'error' => $error
       ];
    }

    /**
     * @Route("/acessar/cadastre-se", name="auth.create")
     * @Template("auth/create.html.twig")
     */
    public function create(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserFrontType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Encode
            $user->setPassword($user->passwordEncode($this->get('security.password_encoder')
            , $form->getData()->getPassword()));

            // Token
            $user->setToken($user->generateToken());

            // Roles
            $user->setRoles("ROLE_ADMIN");

            // Status active
            $user->setStatus(true);

            // Persist
            $this->em->persist($user);
            $this->em->flush();

            // Flash
            $this->addFlash('success', 'Cadastrado com sucesso!');

            // Redirect
            return $this->redirect('/acessar');
        }

        return [
            'form' => $form->createView()
        ];
    }
}
