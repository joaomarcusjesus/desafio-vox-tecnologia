<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAdminCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, ['label' => 'Nome'])
            ->add('last_name', TextType::class, ['label' => 'Sobrenome'])
            ->add('roles', ChoiceType::class, ['label' => 'Nível', 'choices' => [
                'Administrador' => 'ROLE_ADMIN',
                'Gestor' => 'ROLE_GESTOR',
                'Usuário' => 'ROLE_USER'
            ]])
            ->add('status', CheckboxType::class, array(
                'label'    => 'Ativo?',
                'required' => false
            ))
            ->add('email')
            ->add('password', RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => 'As senhas devem ser iguais.',
                    'first_options' => ['label' => 'Senha'],
                    'second_options' => ['label' => 'Confirmar senha'],
                ])
            ->add('enviar', SubmitType::class,
                [
                    'label' => 'Enviar'
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
