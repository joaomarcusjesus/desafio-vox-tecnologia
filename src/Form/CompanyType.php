<?php

namespace App\Form;

use App\Entity\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name' ,TextType::class, ['label' => 'Nome'])
            ->add('email')
            ->add('responsible', TextType::class, ['label' => 'Responsável'])
            ->add('fantasy', TextType::class, ['label' => 'Nome Fantasia'])
            ->add('status', CheckboxType::class, array(
                'label'    => 'Ativo?',
                'required' => false
            ))->add('enviar', SubmitType::class, [
                'label' => 'Enviar'
            ]);
}

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
