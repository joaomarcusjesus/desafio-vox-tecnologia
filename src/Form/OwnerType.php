<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Owner;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OwnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, ['label' => 'Nome'])
            ->add('last_name', TextType::class, ['label' => 'Sobrenome'])
            ->add('email')
            ->add('status', CheckboxType::class, [
                'label'    => 'Ativo?',
                'required' => false
            ])
            ->add('company', EntityType::class, [
                'label' => 'Empresa',
                'class' => Company::class,
                'choice_label' => 'name'
            ])->add('enviar', SubmitType::class, [
                    'label' => 'Enviar'
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Owner::class,
        ]);
    }
}
