# VERSÃO DO SYMFONY 4, usei a nova filosofia do symfony flex, talvez tenha demorado um pouco para estudar a nova versão.
# Autenticacao, usei o básico e tudo nativo, não usei nenhuma bundle ou lib diferente.
# Permissionamento, fiz do jeito mais simples possível por causa do meu tempo, porém, deveria ter feito um is_granted no model para ficar mais organizado.
# Não utilizar gerador de código, fiquei em dúvida sobre essa task, poderia ter utilizado o make? e alguns comandos do doctrine?
# Desenvolver os recursos em API Rest, queria ter um pouco mais de tempo, eu uso bastante o VUE JS e queria brincar com os componentes que eu fiz o no front-end, fiz uma rota bem básica mesmo, para não pasasr em branco ;)

# Vou falar da estrutura ok?

# Eu uso bastante o laravel, e meio que criei um padrão que funciona bem em projetos rápidos.
# Utilizo 3 pastas (as vezes 4 por causa da api), front, auth e admin, cada uma delas tem os seus próprios componentes e assets
# Mesma coisa nos Controllers, gosto de dividir as responsabilidades e deixa cada um no seu canto.
# Algum código que vai ser utilizado em outro controllers, normalmente eu jogo em um service ou em uma API.


# Eu gostei do desafio, fazia um tempo que não pegava no symfony, já que eu praticamente só uso laravel mas claro que a curva de aprendizado é baixa, é bem divertido na verdade.

# Enfim, espero que gostem! foi pouco tempo mas deu para mostrar algo.
# Qualquer feedback aí, é bem vindo!

# Obrigado pela oportunidade


# Commands

# php bin/console server:run
# php bin/console doctrine:database:create
# php bin/console doctrine:migrations:migrate
...

# Routes

# Para acessar a negaveção da autentificação "/acessar"
# Para acessar a navegação do front-end "/"
# para acessar a navegação do admin /"admin/*"


# Contato - joaomarcusjesus@gmail.com